/**
 * Created by bernardo on 19/03/14.
 */


var Mongoose = require('mongoose'),
    mongoose = Mongoose.mongoose,
    ObjectId = Mongoose.Schema.Types.ObjectId;

var handleError = function(error) {
    print("Error on runtime: " + error);
    return false;
};

var Database = function () {
    mongoose.connect('mongodb://localhost/QSearch');
    this.db = mongoose.connection;
    this.db.on('error', console.error.bind(console, 'connection error:'));

    this.urlModel = mongoose.model('Urls',
        mongoose.Schema({
            url: String,
            lastVisit: { type: Date, default: Date.now }
        })
    );

    this.questionModel = mongoose.model('Questions',
        mongoose.Schema({
            url_id: ObjectId,
            question: String
        })
    );

    this.keywordModel = mongoose.model('Keywords',
        mongoose.Schema({
            url_id: ObjectId,
            keyword: String,
            occurrence: Number
        })
    );
};

Database.prototype.save = function(keywords, questions, url) {
    var self = this;

    // Save url if not already present
    self.urlModel.findOne({url: url}).exec(function (err, urlRec) {
        if (err) {
            urlRec = new self.urlModel({ url: url });
            urlRec.save(function (err) {
                if (err)
                    handleError(err);

                // Save questions if not already present
                questions.forEach(function (question) {
                    self.questionModel.findOne({question: question, url_id: urlRec})
                        .exec(function (err, questionRec) {
                        if (err) {
                            questionRec = new self.questionModel({ url_id: urlRec, question: question });
                            questionRec.save(function (err) {
                                if (err)
                                    handleError(err);
                            });
                        }
                    });
                });

                // Save questions if not already present
                keywords.every(function(nb, word) {
                    self.keywordModel.findOne({keyword: word, url_id: urlRec})
                        .exec(function (err, keywordRec) {
                        if (err) {
                            keywordRec = new self.keywordModel({
                                url_id: urlRec,
                                keyword: word,
                                occurrence: nb}
                            );

                            keywordRec.save(function (err) {
                                if (err)
                                    handleError(err);
                            });
                        }
                    });
                });
            });
        }
    });
};

exports.Database = Database;