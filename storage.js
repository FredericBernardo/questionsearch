/**
 * Created by bernardo on 19/02/14.
 *
 */

var Database = require('./database.js').Database;

var Storage = function () {
    this.maxUrlAccepted = 1;
    this.urlListAccepted = [];
    this.urlListStored = [];
    this.database = new Database;
    // DB related
};

Storage.prototype.accept = function( url ) {
    if (this.urlListAccepted.length < this.maxUrlAccepted && this.urlListAccepted.indexOf(url) == -1) {
        this.urlListAccepted.push(url);
        console.log('Accept ' + url);
        return true;
    }
    return false;
};

Storage.prototype.save = function(url, questions, keywords) {
    this.database.save(url, questions, keywords);
    this.urlListStored.push(url);
};

exports.Storage = Storage;