/**
 * Created by bernardo on 19/02/14.
 */

var Crawler = require("crawler").Crawler;
var Storage = require("./storage.js").Storage;
var jquery = require("fs").readFileSync('./public/javascripts/jquery.js', "utf-8");

var Parser = function(io) {
    var self = this;
    this.io = io;
    this.request = new Crawler({
        maxConnections: 10,
        src: jquery,

        // This will be called for each crawled page
        callback: function(error, result, $) {
            if (error && response.statusCode !== 200) {
                console.log('Error when contacting ' + url )
            }
            else {
                console.log('Parsing ' + result.request.href);
                self.parse(result.request.href, $);
            }
        }
    });

    this.storage = new Storage();
    this.io.sockets.on('connection', function(socket) {
        socket.emit('begin', {msg:'Begin parsing'});
    });
};

Parser.prototype.run = function(url) {
    if (this.storage.accept(url)) {
        this.request.queue(url);
    }
};

Parser.prototype.parse = function(url, $){
    var self = this;

    self.io.sockets.emit('parse', {url: url});

    // Crawl other links
    $("a").each( function(index, a ) {
        self.run(a.href.match(/(^[^#]*)/)[0]);
    });

    // Get all question of the page
    var questions = parseQuestions( $("*:contains('?')").text() );

    // If any, get every keyword and store
    if (questions.length) {
        var keywords = parseKeywords($("body").text());
        if (keywords.length > 10*questions.length) {
            self.storage.save( url, questions, keywords);
        }
    }



    console.log('done ' + url);
};

// todo Implements a exclude keywords list
function parseKeywords( str ) {
    var pattern = /\b\w{4,20}\b/mg;
    var keywords = {};
    while ( res = pattern.exec( str ) ) {
        keywords[res]++;
    }
    return keywords;
}

function parseQuestions( str ) {
    var pattern = /[A-Z][:^\.!;?]*\?/mg;
    var questions = [];
    while ( res = pattern.exec( str ) ) {
        questions.push( res );
    }
    return questions;
}

exports.Parser = Parser;