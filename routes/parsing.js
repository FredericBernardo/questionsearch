
/*
 * GET parsing page.
 */

var Parser = require('../parser.js').Parser;

exports.run = function(req, res, io) {

    res.render('parsing', {
        title: 'Here is thy parsing page'
    });

    if (req.query.url) {
        var parser = new Parser(io);
        parser.run(req.query.url);
    }


};
